﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStar1 : MonoBehaviour
{

	public Transform m_tSeeker, m_tTarget;
	PathGridManager m_Grid;
    

    void Awake()
	{
		m_Grid = GetComponent<PathGridManager>();
	}

	// Update is called once per frame
	void Update()
	{
		FindPath(m_tSeeker.position, m_tTarget.position);
	}

	void FindPath(Vector3 startPos, Vector3 endPos)
	{

        Node startNode = m_Grid.NodeFromWorldPos(m_tSeeker.position);
        Node endNode = m_Grid.NodeFromWorldPos(m_tTarget.position);

        List<Node> openSet = new List<Node>();
        HashSet<Node> closedSet = new HashSet<Node>();

        openSet.Add(startNode);

        while (openSet.Count > 0)
        {
            Node currentNode = openSet[0];


            for (int i = 0; i < openSet.Count; i++)
            {
                if (openSet[i].m_ifCost < currentNode.m_ifCost || openSet[i].m_ifCost == currentNode.m_ifCost)
                {
                    if (openSet[i].m_iHCost < currentNode.m_iHCost)
                        currentNode = openSet[i];
                }

                openSet.Remove(currentNode);
                closedSet.Add(currentNode);


                if (currentNode == endNode)
                {

                    RetracePath(startNode, endNode);

                    return;
                }

                List<Node> neighbours = m_Grid.GetNeighbours(currentNode);

                foreach (Node n in neighbours)
                {

                    if (n.m_bIsBlocked || closedSet.Contains(n))
                    {

                        continue;
                    }

                    int NewMovementCost = currentNode.m_iGCost + GetDistance(currentNode, n);


                    if (NewMovementCost < n.m_iGCost || !openSet.Contains(n))
                    {

                        n.m_iGCost = NewMovementCost;

                        n.m_iHCost = GetDistance(n, endNode);

                        n.m_Parent = currentNode;


                        if (!openSet.Contains(n))
                        {
                            openSet.Add(n);
                        }
                    }
                }
            }
        }

        

			//PathGridManager can then check if m_aPath is set
			//if it is, it can update the OnDrawGizmos function to check if node is in m_aPath and draw it a different colour.
		
	}

	public void RetracePath(Node start, Node end)
	{
		List<Node> aPath = new List<Node>();
		Node currentNode = end;

		while (currentNode != start)
		{
			aPath.Add(currentNode);
			currentNode = currentNode.m_Parent;
		}

		aPath.Reverse();
		m_Grid.m_aPath = aPath;
	}


	public int GetDistance(Node a, Node b)
	{
		int iDistX = Mathf.Abs(a.m_iGridX - b.m_iGridX);
		int iDistY = Mathf.Abs(a.m_iGridY - b.m_iGridY);

		if (iDistX > iDistY)
			return 14 * iDistY + 10 * (iDistX - iDistY);
		else
			return 14 * iDistX + 10 * (iDistY - iDistX);
	}
}
